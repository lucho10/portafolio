import React from 'react';

import { Section, SectionText, SectionTitle } from '../../styles/GlobalComponents';
import Button from '../../styles/GlobalComponents/Button';
import { LeftSection,Img} from './HeroStyles';

const Hero = (props) => (
  <>
    <Section row nopadding>
      <LeftSection>
        <SectionTitle main center>
          Bienvenido a <br />
          Mi portafolio personal
        </SectionTitle>
        <Img src="/images/profile.PNG" />
      </LeftSection>
    </Section>
  </>
);

export default Hero;