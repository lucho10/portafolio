export const projects = [
  {
    title: 'FincApp',
    description: "Una Aplicacion para el alquiler de fincas en el Quindio",
      image: '/images/fincApp.jpeg',
      tags: ['Firebase', 'Swift', 'Kotlin'],
    id: 0,
  },
];

export const TimeLineData = [
  { year: 2020, text: 'Ingeniero de software en Celuweb', },
  { year: 2021, text: 'Desarrollador de software en Hellobuild', },
];